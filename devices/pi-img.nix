{ pkgs ? import <nixpkgs> {}}:
pkgs.callPackage ../buildOpenwrt.nix {
        target = "bcm27xx";
        arch = "bcm2708";
        version= "23.05.3";
        profile = "rpi";
        packages = ["luci-ssl"];       
}
