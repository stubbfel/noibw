{
  description = "NOIBW - Nix-Openwrt-ImageBuilder-Wrapper";
  inputs.nixpkgs.url = "nixpkgs/nixos-23.11";

  outputs = { self, nixpkgs }:
    let
      name = "NOIBW";
      version = "1.7.0";
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });
      devTaskScripts = forAllSystems (system:
        {
          autoTag = nixpkgsFor.${system}.writeScript "auto_tag.sh" ''
            git tag --force v${version}
            git push origin v${version}
          '';
        });
      appScripts = forAllSystems (system:
        {
          makeCleanImageScript = nixpkgsFor.${system}.writeScript "makeCleanImage.sh" ''
            nix-shell "$1" --run makeCleanImage
          '';
        });
    in
    rec {
      packages = forAllSystems (system:
        {
          default = appScripts.${system}.makeCleanImageScript;
        });

      apps = forAllSystems (system:
        let
          updateLockScript = nixpkgsFor.${system}.writeShellScriptBin "update_flake_lock.sh" ''
            nix --experimental-features 'nix-command flakes' flake lock --update-input nixpkgs
            nix --experimental-features 'nix-command flakes' build 
          '';
        in
        {
          default = { type = "app"; program = "${packages.${system}.default}"; };
          devTasks = {
            updateFlakeLock = { type = "app"; program = "${updateLockScript}/bin/update_flake_lock.sh"; };
            autoTag = { type = "app"; program = "${devTaskScripts.${system}.autoTag}"; };
          };
        });

      devShells = forAllSystems (system:
        {
          default = nixpkgsFor.${system}.mkShell {
            name = "dev-shell";
            packages = with  nixpkgsFor.${system}; [
              perl
              quilt
              ccache
              libxslt
              gcc
              gengetopt
              subversion
              git
              python3Full
              rsync
              man-db
              gawk
              gettext
              unzip
              file
              wget
              ncurses5.dev
              zlib.static
              gnumake
              which
              bash
              coreutils
              wget
              gnutar
              curl
              cacert
            ];
            shellHook = ''
              alias nixe="nix --experimental-features 'nix-command flakes'"
            '';
          };
        });      
    };
}
