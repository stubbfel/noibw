{
    pkgs ? import <nixpkgs> {},
    target,
    arch,
    profile,
    version,
    packages ? [],
    manifestPkgs ? "$(cat manifest_packages)",
    workingFolder ? "$PWD/_builder",
    builderVersion ? version,
    moreBuildInputs ? []
}:
pkgs.mkShell {
    name = "openwrt-build-${target}-${arch}-${profile}";
    buildInputs = with  pkgs; [
        perl
        quilt
        ccache
        libxslt
        gcc
        gengetopt
        subversion
        git
        python3Full
        rsync
        man-db
        gawk
        gettext
        unzip
        file
        wget
        ncurses5.dev
        zlib.static
        gnumake
        which
        bash
        coreutils
        wget
        gnutar
        curl
        cacert
    ] ++ moreBuildInputs;
    shellHook =
    ''
        export WORKING_FOLDER=$(realpath ${workingFolder})
        mkdir -p $WORKING_FOLDER
        function loadBuilder() {
            curl -s https://downloads.openwrt.org/releases/${builderVersion}/targets/${target}/${arch}/openwrt-imagebuilder-${builderVersion}-${target}-${arch}.Linux-x86_64.tar.xz | tar xvJ -C $WORKING_FOLDER
            curl -s https://downloads.openwrt.org/releases/${version}/targets/${target}/${arch}/openwrt-${version}-${target}-${arch}.manifest | cut -f 1 -d ' ' | tr '\n' ' ' > $WORKING_FOLDER/openwrt-imagebuilder-${builderVersion}-${target}-${arch}.Linux-x86_64/manifest_packages
        }

        function removeBuilder() {
            rm -rf $WORKING_FOLDER/openwrt-imagebuilder-${builderVersion}-${target}-${arch}.Linux-x86_64
        }

        function makeImage() {
            cd $WORKING_FOLDER/openwrt-imagebuilder-${builderVersion}-${target}-${arch}.Linux-x86_64
            MANIFEST_PKGS=${manifestPkgs}
            make image PROFILE=${profile} PACKAGES="$MANIFEST_PKGS ${toString packages}"
        }

        function makeCleanImage() {
            removeBuilder && loadBuilder && makeImage
        }
    '';
}
